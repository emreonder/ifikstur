//
//  CustomCollectionViewCell.swift
//  iFikstur
//
//  Created by Emre Onder on 13/11/17.
//  Copyright © 2017 Emre Onder. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mImageView: UIImageView!
    @IBOutlet weak var played: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    
}
