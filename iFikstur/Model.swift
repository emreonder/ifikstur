//
//  Model.swift
//  iFikstur
//
//  Created by Emre Onder on 10/10/17.
//  Copyright © 2017 Emre Onder. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
protocol ModelDelegate {
    func errorUpdating(_ error: NSError)
    func modelUpdated()
}
class Model: NetworkDelegate{
    //Function which gets JSON Array from Network Layer and Assign it to Model
    func sendJson(_ get_json: [JSON]){
    title = [String]()
    for k in 0..<get_json.count{
        var first_array = [Team]()
        var json = get_json[k]
        if let error_json =  json["Error"].string {
                let dataFetch: [AnyHashable : Any] =
                    [
                        NSLocalizedDescriptionKey :  NSLocalizedString("Hata", value: error_json, comment: "") ,
                        NSLocalizedFailureReasonErrorKey : NSLocalizedString("Hata", value: error_json, comment: "")            ]
                let err = NSError(domain: "ShiploopHttpResponseErrorDomain", code: 401, userInfo: dataFetch)
                self.delegate?.errorUpdating(err as NSError)
            return
        }else{
        title?.append(json["LeagueStage"][0]["TournamentName"].string!)
        for i in 0..<json["LeagueStage"][0]["LeagueTable"].count {
            guard let name = json["LeagueStage"][0]["LeagueTable"][i]["name"].string,
                let id = json["LeagueStage"][0]["LeagueTable"][i]["id"].int,
                let teamId = json["LeagueStage"][0]["LeagueTable"][i]["TeamID"].int,
                let points = json["LeagueStage"][0]["LeagueTable"][i]["points"].string,
                let played = json["LeagueStage"][0]["LeagueTable"][i]["played"].string,
                let wins = json["LeagueStage"][0]["LeagueTable"][i]["wins"].string,
                let draws = json["LeagueStage"][0]["LeagueTable"][i]["draws"].string,
                let defeits = json["LeagueStage"][0]["LeagueTable"][i]["defeits"].string,
                let goalsfor = json["LeagueStage"][0]["LeagueTable"][i]["goalsfor"].string,
                let goalsagainst = json["LeagueStage"][0]["LeagueTable"][i]["goalsagainst"].string,
                let average = json["LeagueStage"][0]["LeagueTable"][i]["average"].int,
                let rank = json["LeagueStage"][0]["LeagueTable"][i]["rank"].int,
                let hasTeamLogo = json["LeagueStage"][0]["LeagueTable"][i]["HasTeamLogo"].bool,
                let imageUrl = json["LeagueStage"][0]["LeagueTable"][i]["TeamLogo"].string
                else {
                    let dataFetch: [AnyHashable : Any] =
                        [
                            NSLocalizedDescriptionKey :  NSLocalizedString("Veri Yok", value: "Hata", comment: "") ,
                            NSLocalizedFailureReasonErrorKey : NSLocalizedString("Veri Yok", value: "Hata", comment: "")            ]
                    let err = NSError(domain: "ShiploopHttpResponseErrorDomain", code: 401, userInfo: dataFetch)
                    self.delegate?.errorUpdating(err as NSError)
                    return
            }
            let temp_team = Team(id: id,
                                 teamId: teamId,
                                 name: name,
                                 points: Int(points)!,
                                 played: Int(played)!,
                                 wins: Int(wins)!,
                                 draws: Int(draws)!,
                                 defeits: Int(defeits)!,
                                 goalsfor: Int(goalsfor)!,
                                 goalsagainst: Int(goalsagainst)!,
                                 average: average,
                                 rank: rank,
                                 HasTeamLogo: hasTeamLogo,
                                 imageurl: imageUrl
            )
                first_array.append(temp_team)
        }
    }
        self.teams[k] = first_array
        }
        self.delegate?.modelUpdated()
    }

    
    // MARK: - Variables
    static let sharedInstance = Model()
    var delegate: ModelDelegate?
    var teams = [[Team]]()
    var title: [String]?
    init()
    {
    }
    
    @objc func refresh_fiks(){
        //Creates Network Layer and Send Requests
        let network = Network()
        network.delegate = self
        self.teams = [[],[]]
        network.getMultipleRequests([1,2])
    }
}
