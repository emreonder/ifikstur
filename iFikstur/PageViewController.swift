//
//  PageViewController.swift
//  iFikstur
//
//  Created by Emre Onder on 10/11/17.
//  Copyright © 2017 Emre Onder. All rights reserved.
//

import UIKit
import ChameleonFramework
// The page view controller and data source that configures and manages the presentation of paginated view controllers.
class PageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    // The storyboard identifier for the view controller being paginated.
    let viewControllerIdentifier = "TableViewController"
    // The number of pages being presented.
    var pageCount = 2
    // The source of the data.
    let model: Model = Model.sharedInstance


  // Configures self as the data source, and installs the page indicator and first-presented view controller.
  override func viewDidLoad() {

    super.viewDidLoad()
    self.delegate = self
    //Navigation Bar Attributes
    self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont.systemFont(ofSize: 23.0, weight: UIFontWeightHeavy), NSForegroundColorAttributeName: UIColor.flatWhite]
    self.navigationController?.navigationBar.tintColor = UIColor.flatWhite
    self.navigationController?.navigationBar.isTranslucent = false
    self.navigationController?.navigationBar.apply(gradient: [UIColor.flatSkyBlue, UIColor.flatGreen])
    if let title = model.title?[0]{
        self.navbar_animation(title: title)
    }else{
        self.navigationController?.topViewController?.title = "iFikstur"
    }
    
    //Model
    model.delegate = self
    
    // Install this class as the data source
    dataSource = self
    
    //View Attributes
    self.view.backgroundColor = UIColor.flatBlack
    
    // Configure page indicator dot colors
    let pageControl = UIPageControl.appearance(whenContainedInInstancesOf: [PageViewController.self])
    pageControl.pageIndicatorTintColor = UIColor.flatGrayDark
    pageControl.currentPageIndicatorTintColor = UIColor.flatWhite
    
    // Install the first-presented view controller
    let tvc = storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifier) as! TableViewController
    tvc.section = 0
    tvc.model = model.teams
    setViewControllers([tvc], direction: .forward, animated: true, completion: nil)

    //Automatically Refresh Table in every 2 min
    let myTimer = Timer(timeInterval: 120.0, target: self, selector: #selector(PageViewController.reloadData), userInfo: nil, repeats: true)
    RunLoop.main.add(myTimer, forMode: RunLoopMode.defaultRunLoopMode)
    
  }
    //Mark: - Refresh Model
    func reloadData(){
        model.refresh_fiks()
    }
    
  // Returns a new `TableViewController` configured with the given `model` and `section`.
  func newTableViewController(forSection section: Int, of model: [[Team]]) -> TableViewController {
    let newTVC = storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifier) as! TableViewController
    newTVC.model = model
    newTVC.section = section
    return newTVC
  }
    // Returns a new `TableViewController` configured with the given `model` and `section`.
    func newCollectionViewController(forSection section: Int, of model: [[Team]]) -> CollectionViewController {
        let newCVC = storyboard?.instantiateViewController(withIdentifier: "CollectionViewController") as! CollectionViewController
        newCVC.model = model
        newCVC.section = section
        return newCVC
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    
        if (!completed) {
            return
        }
        
        if (viewControllers?.first) != nil{
            let arrayIndex = self.presentationIndex(for: self)
            switch arrayIndex {
            case 0:
                self.navbar_animation(title: (model.title?[0])!)
                break
                
            case 1:
                self.navbar_animation(title: (model.title?[1])!)
                break
                
            default:
                self.navbar_animation(title: "iFikstur")
                
                
            }
        }
    }
         // Returns the index of the currently visible view controller.
  func presentationIndex(for pageViewController: UIPageViewController) -> Int {
    if let viewControllers = viewControllers, viewControllers.count > 0 {
        if viewControllers[0] is TableViewController{
      return (viewControllers[0] as! TableViewController).section!
        }else{
            return (viewControllers[0] as! CollectionViewController).section!

        }
    }
    return 0
  }

    //Nav Bar Animation
    func navbar_animation(title: String){
        let fadeTextAnimation = CATransition()
        fadeTextAnimation.duration = 0.5
        fadeTextAnimation.type = kCATransitionFade
        navigationController?.navigationBar.layer.add(fadeTextAnimation, forKey: "fadeText")
        self.navigationController?.topViewController?.title = title
    }
  // MARK: - Page View Controller Data Source

  // Returns the _next_ view controller, or `nil` if there is no such controller.
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
    if viewController is TableViewController{
        let cvc = viewController as! TableViewController
       
        return cvc.section! < pageCount - 1 ? newCollectionViewController(forSection: cvc.section! + 1, of: model.teams) : nil
    }else{
        let cvc = viewController as! CollectionViewController
        return cvc.section! < pageCount - 1 ? newCollectionViewController(forSection: cvc.section! + 1, of: model.teams) : nil
    }
    }

  // Returns the _previous_ view controller, or `nil` if there is no such controller.
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    let tvc = viewController as! TableViewController
    return tvc.section! > 0 ? newTableViewController(forSection: tvc.section! - 1, of: model.teams) : nil
  }


  // Returns the number of pages to represent in the `UIPageControl` page indicator.
  func presentationCount(for pageViewController: UIPageViewController) -> Int {
    return pageCount
  }
}
// MARK: - ModelDelegate
extension PageViewController: ModelDelegate {
    
    func modelUpdated() {
        //Notify TableViewController Class
        let userInfo = [ "passed_data" : model.teams ]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "refresh"), object: nil, userInfo: userInfo)

    }
    func errorUpdating(_ error: NSError) {
        let message: String
        if error.code == 1 {
            message = "Error"
        } else {
            message = error.localizedDescription
        }
        let alertController = UIAlertController(title: nil,
                                                message: message,
                                                preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Tekrar Dene", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
  }
