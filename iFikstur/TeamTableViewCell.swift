//
//  TeamTableViewCell.swift
//  iFikstur
//
//  Created by Emre Onder on 10/10/17.
//  Copyright © 2017 Emre Onder. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell {

    @IBOutlet weak var rank: UILabel!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var points: UILabel!
    @IBOutlet weak var week: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
