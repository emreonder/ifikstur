//
//  Team.swift
//  iFikstur
//
//  Created by Emre Onder on 8/27/17.
//  Copyright © 2017 Emre Onder. All rights reserved.
//

import Foundation
class Team: NSObject{
    // MARK: - Properties
    var id: Int?
    var TeamId: Int?
    var name: String?
    var points: Int?
    var played: Int?
    var wins: Int?
    var draws: Int?
    var defeits: Int?
    var goalsfor: Int?
    var goalsagainst: Int?
    var average: Int?
    var rank: Int?
    var HasTeamLogo: Bool?
    var imageURL: String?
    
    init(id: Int, teamId: Int,name: String,points: Int,played: Int,wins: Int, draws: Int, defeits: Int, goalsfor:Int, goalsagainst: Int, average: Int,rank: Int,HasTeamLogo: Bool, imageurl: String) {
        self.id = id
        self.TeamId = teamId
        self.name = name
        self.points = points
        self.played = played
        self.wins = wins
        self.draws = draws
        self.defeits = defeits
        self.goalsfor = goalsfor
        self.goalsagainst = goalsagainst
        self.average = average
        self.rank = rank
        self.HasTeamLogo = HasTeamLogo
        self.imageURL = imageurl
    }
    override init(){
        self.id = nil
        self.TeamId = nil
        self.name = nil
        self.points = nil
        self.played = nil
        self.wins = nil
        self.draws = nil
        self.defeits = nil
        self.goalsfor = nil
        self.goalsagainst = nil
        self.average = nil
        self.rank = nil
        self.HasTeamLogo = nil
    }
    
}
