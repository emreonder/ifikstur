//
//  MainViewController.swift
//  CollectionViewMVC_SW
//
//  Created by Ctis on 08/12/16.
//  Copyright © 2016 CTIS. All rights reserved.
//

import UIKit
import ChameleonFramework
class CollectionViewController: UICollectionViewController{
    
    var section: Int?
    /// The source of the data.
    var model: [[Team]] = []
    var refreshControl:UIRefreshControl!
    let model_ref: Model = Model.sharedInstance
    var reachability = Reachability()!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Shifting the mCollectionView to up by -10 units
        //self.mCollectionView.contentInset = UIEdgeInsetsMake(-10.0, 0.0, 0.0, 0.0)
        
        // The distance the scroll indicators are inset from the edge of the scroll view. The default value is UIEdgeInsetsZero.
        //self.mCollectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 10.0)
        collectionView?.backgroundColor = UIColor.flatBlack
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(CollectionViewController.handleRefresh), for: .valueChanged)
        collectionView!.addSubview(refreshControl)
        //Notify TableView When Model Updated
        if(model.count > 0){
            NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.refreshCollection(_:)), name: NSNotification.Name(rawValue: "refresh"), object: nil)
        }
    }
    func handleRefresh(){
        if(checkInternet()){
            model_ref.refresh_fiks()
        }else{
            let alertController = UIAlertController(title: nil,
                                                    message: "İnternet Bağlantısı Gerekmektedir!",
                                                    preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "Tamam", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
            if #available(iOS 10.0, *) {
                self.refreshControl?.endRefreshing()
            } else {
                // Fallback on earlier versions
            }
        }
    }
    //Internet Check
    func checkInternet() -> Bool{
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
            } else {
            }
        }
        //NO Internet
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        switch reachability.connection {
        case .wifi:
            print("Reachable via WiFi")
            return true
        case .cellular:
            print("Reachable via Cellular")
            return true
        case .none:
            print("Network not reachable")
            return false
        }
    }

    //Refresh Collection With New Model
    func refreshCollection(_ notification: Notification) {
        guard (notification.userInfo?["passed_data"] as? [[Team]]) != nil else{
            return
        }
        model = [[]]
        model = Model.sharedInstance.teams
        self.collectionView?.reloadData()
        self.refreshControl?.endRefreshing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return model[section].count
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        if (model.count > 0){
            return 1
        }else{
            return 0
        }    }
    
    // For each cell setting the data
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CustomCollectionViewCell
        
        let object = model[section!][(indexPath as NSIndexPath).row]
        cell.rankLabel.text = "\(object.rank!) - \(String(describing: object.name!))"
        cell.rankLabel.adjustsFontSizeToFitWidth = true
        cell.played.text = "Played: \(String(describing: object.played!))"
        cell.pointsLabel.text = "Points: \(String(describing: object.points!))"
        if(object.HasTeamLogo == true){
            let url = URL(string: object.imageURL!)
            cell.mImageView.kf.setImage(with: url)
        }else{
            cell.mImageView.image = UIImage(named: "default-team-logo-500")
        }
        cell.backgroundColor = UIColor.flatSkyBlue.withAlphaComponent(0.5)
        cell.layer.cornerRadius = 5.0
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = getIndexPathForSelectedCell() {
            
            let destination = segue.destination as! DetailViewController
            
            destination.destObject = [section!, indexPath.row]
        }

    }
    
    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any?) -> Bool {
        return !isEditing
    }
    
    // Our function to find the indexPath of selected cell
    func getIndexPathForSelectedCell() -> IndexPath? {
        var indexPath: IndexPath?
        
        if (collectionView?.indexPathsForSelectedItems!.count)! > 0 {
            indexPath = collectionView?.indexPathsForSelectedItems![0]
        }
        
        return indexPath
    }
    
    // Selecting/Highlighting a selected Cell
    override func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        highlightCell(indexPath, flag: true)
    }
    
    // Deselecting/Dehighlighting a selected Cell
    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        highlightCell(indexPath, flag: false)
    }
    
    // Our function to perform highlight and dehighlight
    func highlightCell(_ indexPath : IndexPath, flag: Bool) {
        
        let cell = collectionView?.cellForItem(at: indexPath)
            cell?.contentView.backgroundColor = UIColor(gradientStyle: UIGradientStyle.leftToRight, withFrame:(cell?.frame)!, andColors:[UIColor.flatGreen,UIColor.flatSkyBlue]).withAlphaComponent(0.5)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            cell?.contentView.backgroundColor = UIColor.flatSkyBlue.withAlphaComponent(0.0)
        })

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if let indexPath = getIndexPathForSelectedCell() {
            highlightCell(indexPath, flag: false)
        }
    }
    
   

}

