//
//  TableViewController.swift
//  iFikstur
//
//  Created by Emre Onder on 10/11/17.
//  Copyright © 2017 Emre Onder. All rights reserved.
//

import UIKit
import Kingfisher
import ChameleonFramework
/// A table view controller that presents sections of `Model` data for aginated views.

class TableViewController: UITableViewController {

  var section: Int?

  /// The source of the data.
    var model: [[Team]] = []
    let model_ref: Model = Model.sharedInstance
    var reachability = Reachability()!

    override func viewDidLoad() {
        
        // Table View Settings.
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.flatBlack
       if #available(iOS 10.0, *) {
            tableView.refreshControl = UIRefreshControl()
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 10.0, *) {
                tableView.refreshControl?.addTarget(self, action: #selector(TableViewController.handleRefresh), for: .valueChanged)
        } else {
            // Fallback on earlier versions
        }
        
        //Notify TableView When Model Updated
        if(model.count > 0){
            NotificationCenter.default.addObserver(self, selector: #selector(TableViewController.refreshTable(_:)), name: NSNotification.Name(rawValue: "refresh"), object: nil)
        }
    }
    func handleRefresh(){
        if(checkInternet()){
            model_ref.refresh_fiks()
        }else{
            let alertController = UIAlertController(title: nil,
                                                    message: "İnternet Bağlantısı Gerekmektedir!",
                                                    preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "Tamam", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
            if #available(iOS 10.0, *) {
                tableView.refreshControl?.endRefreshing()
            } else {
                // Fallback on earlier versions
            }
        }
    }
    //Refresh Table With New Model
    func refreshTable(_ notification: Notification) {
        guard (notification.userInfo?["passed_data"] as? [[Team]]) != nil else{
        return
        }
        model = [[]]
        model = Model.sharedInstance.teams
        self.tableView.reloadData()
        if #available(iOS 10.0, *) {
            tableView.refreshControl?.endRefreshing()
        } else {
            // Fallback on earlier versions
        }
    }
    //Internet Check
    func checkInternet() -> Bool{
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
            } else {
            }
        }
        //NO Internet
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        switch reachability.connection {
        case .wifi:
            print("Reachable via WiFi")
            return true
        case .cellular:
            print("Reachable via Cellular")
            return true
        case .none:
            print("Network not reachable")
            return false
        }
    }

  // MARK: - Table View Data Source

  // Returns the number of sections in the table.
  override func numberOfSections(in tableView: UITableView) -> Int {
    if (model.count > 0){
        return 1
    }else{
        return 0
    }
  }


  // Returns the number of rows in the requested table `section`.
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return model[section].count
  }
  // Returns a cell for the requested `indexPath` populated with the corresponding data.
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TeamTableViewCell
    let object = model[section!][(indexPath as NSIndexPath).row]
    
    cell.teamName.text = object.name
    cell.teamName.textColor = UIColor.flatWhite
    cell.rank.text = String(describing: object.rank!)
    cell.rank.textColor = UIColor.flatWhite
    cell.points.text = String(describing: object.points!)
    cell.points.textColor = UIColor.flatWhite
    cell.week.text = String(describing: object.played!)
    cell.week.textColor = UIColor.flatWhite
    if(object.HasTeamLogo == true){
        let url = URL(string: object.imageURL!)
        cell.teamImage.kf.setImage(with: url)
    }else{
        cell.teamImage.image = UIImage(named: "default-team-logo-500")
    }
    cell.backgroundColor = .clear
    return cell
  }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
  /// Stub for responding to user row selection.
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  }
    // Send Section and Row Number to DetailViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? DetailViewController {
            if let selectedRowIndexPath = tableView.indexPathForSelectedRow?.row {
                
                destination.destObject = [section!, selectedRowIndexPath]
            }
        }
    }
}
