//
//  Network.swift
//  iFikstur
//
//  Created by Emre Onder on 13/13/17.
//  Copyright © 2017 Emre Onder. All rights reserved.
//
import Alamofire
import SwiftyJSON
protocol NetworkDelegate {
    func sendJson(_ json: [JSON])
}
struct Router: URLRequestConvertible {
    
    // Initialize Variables
    static private let baseUrlString = "http://www.posta.com.tr/api/LiveScore/LeagueStage?TournamentID="
    static private let baseUrl2 = "&includeTable=1"
    
    static var OAuthToken: String?
    var path: Int!
    var parameters: Parameters?
    var method: Alamofire.HTTPMethod!
    
    init(method: Alamofire.HTTPMethod, path: Int, parameters: Parameters?) {
        self.method = method
        self.parameters = parameters
        self.path = path
    }
    
    func asURLRequest() throws -> URLRequest {
        let url_string = "\(Router.baseUrlString)\(path!)\(Router.baseUrl2)"
        let url = URL(string: url_string)
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = method.rawValue
        if let token = Router.OAuthToken {
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        if self.method == Alamofire.HTTPMethod.post {
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: self.parameters!)
        } else if self.method == Alamofire.HTTPMethod.put {
            return try Alamofire.URLEncoding.default.encode(urlRequest, with: self.parameters)
        } else {
            return urlRequest
        }
    }
}
//Network Class
class Network{
    
    // Initialize Variables
    var delegate: NetworkDelegate?
    var response_json: [JSON] = []
    var path: Int?
    //Func to get Multiple Requests and Wait for all of them to finish
    func getMultipleRequests(_ requests: [Int]) {
        DispatchQueue.global(qos: .background).async {
            let group = DispatchGroup()
            
            var array: [JSON] = []
            for request in requests {
                group.enter()
                let second_group = DispatchGroup()
                second_group.enter()
                self.getRequest(req: request, completion: { (json) in
                    array.append(json)
                    second_group.leave()
                    group.leave()
                })
                second_group.wait()
            }
            
            group.notify(queue: DispatchQueue.main, execute: {
                print("All Done")
                self.delegate?.sendJson(array)
            })
        }
    }
    //Alamofire Request Function
    func getRequest(req: Int, completion: @escaping (_ json: JSON) -> Void) {
        path = req
        let rot = Router(method: .get, path: req, parameters: nil)
        Alamofire.request(rot)
            .response { response in
                // check for errors
                guard response.error == nil else {
                    // got an error in getting the data, need to handle it
                    print(response.error!)
                    let errorJson: JSON = [ "Error" : "Can't get the data!"]
                    completion(errorJson)
                    return
                }
                // make sure we got some JSON since that's what we expect
                guard (response.data?.base64EncodedString()) != nil else {
                    print("Error: \(String(describing: response.error))")
                    let errorJson: JSON = [ "Error" : "Can't get the data!"]
                    completion(errorJson)
                    return
                }
                guard response.response?.statusCode == 200 else{
                    let errorJson: JSON = [ "Error" : "Network Error!"]
                    completion(errorJson)
                    return
                }
                let json = JSON(data: response.data!)
                // get and print the title
                if json != nil{
                    completion(json)
                } else {
                    let errorJson: JSON = [ "Error" : "Can't get the data!"]
                    completion(errorJson)
                    return
                }
        }
    }
}

