//
//  MainViewController.swift
//  iFikstur
//
//  Created by Emre Onder on 10/11/17.
//  Copyright © 2017 Emre Onder. All rights reserved.
//

import UIKit
import ChameleonFramework
import Lottie
import LGButton
class MainViewController: UIViewController {
    
    // MARK: - Outlets and Variables
    @IBOutlet weak var errorText: UILabel!
    let model: Model = Model.sharedInstance
    var reachability = Reachability()!
    let animationView = LOTAnimationView(name: "animation")
    var flag = false
    @IBOutlet var errorView: UIView!
    @IBOutlet weak var biglabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Navigation Bar Attributes
        self.title = "iFikstür"
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont.systemFont(ofSize: 23.0, weight: UIFontWeightHeavy), NSForegroundColorAttributeName: UIColor.flatWhite]
        self.navigationController?.navigationBar.tintColor = UIColor.flatWhite
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.apply(gradient: [UIColor.flatSkyBlue, UIColor.flatGreen])
        
        //Error View Properties
        biglabel.adjustsFontSizeToFitWidth = true
        self.errorView.backgroundColor = UIColor.flatBlack
        self.view.backgroundColor = UIColor.flatBlack
        self.errorView.isHidden = true
        errorText.adjustsFontSizeToFitWidth = true
        //Animation Frame
        let barHeight = (self.navigationController?.navigationBar.frame.height)! / 2 
        animationView.contentMode = .scaleAspectFit
        animationView.loopAnimation = true
        view.addSubview(animationView)
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -barHeight).isActive = true
        animationView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true

        //Start Animation
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(MainViewController.animation_play), userInfo: nil, repeats: false)
    }
    func animation_play(){
        if(checkInternet()){
            //Call Model Functions
            self.model.delegate = self
            Model.sharedInstance.teams = [[]]
            self.model.refresh_fiks()
            self.errorView.isHidden = true
        }else{
            self.errorView.isHidden = false
            animationView.removeFromSuperview()
        }
    }
    //Internet Check
    func checkInternet() -> Bool{
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
            } else {
            }
        }
        //NO Internet
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        switch reachability.connection {
        case .wifi:
            print("Reachable via WiFi")
            return true
        case .cellular:
            print("Reachable via Cellular")
            return true
        case .none:
            print("Network not reachable")
            return false
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        //Play Animation
        //self.animationView.play(completion: {(_ animationFinished: Bool) -> Void in})
        self.animationView.play()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Error Button Function
    @IBAction func touched(_ sender: LGButton) {
        if(checkInternet()){
            if(flag == false){
                self.model.delegate = self
                Model.sharedInstance.teams = [[]]
                self.model.refresh_fiks()
                view.addSubview(animationView)
                let barHeight = (self.navigationController?.navigationBar.frame.height)! / 2
                animationView.translatesAutoresizingMaskIntoConstraints = false
                animationView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -barHeight).isActive = true
                animationView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
                animationView.play()
                self.animationView.isHidden = false
                self.errorView.isHidden = true
                flag = true
            }
            //Call Model Functions
                   }else{
        }
    }
}
// MARK: - ModelDelegate
extension MainViewController: ModelDelegate {
    // Model Updated - Go PageViewController
    func go(){
        //Finish Animation
        animationView.removeFromSuperview()
        
        //Create Container View to Show PageViewController
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(containerView)
        
        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            containerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            ])
        
        containerView.backgroundColor = UIColor.white
        // add child view controller view to container
        let controller = storyboard?.instantiateViewController(withIdentifier: "PageViewSeg") as! PageViewController
        addChildViewController(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(controller.view)
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
            ])
        
        controller.didMove(toParentViewController: self)
        
    }
    
    func modelUpdated() {
        //Delayed to show Animation
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            self.go()
        })
    }
    //Show Message If model has problems with loading
    func errorUpdating(_ error: NSError) {
        let message: String
        if error.code == 1 {
            message = "Error"
        } else {
            message = error.localizedDescription
        }
        let alertController = UIAlertController(title: nil,
                                                message: message,
                                                preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Okey", style: .default, handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
}
extension UINavigationBar
{
    /// Applies a background gradient with the given colors
    func apply(gradient colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    
    /// Creates a gradient image with the given settings
    static func gradient(size : CGSize, colors : [UIColor]) -> UIImage?
    {
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        // From now on, the context gets ended if any return happens
        defer { UIGraphicsEndImageContext() }
        
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: size.width, y: 0.0), options: [])
        
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
