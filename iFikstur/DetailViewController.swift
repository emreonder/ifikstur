//
//  DetailViewController.swift
//  iFikstur
//
//  Created by Emre Onder on 10/11/17.
//  Copyright © 2017 Emre Onder. All rights reserved.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController {

    //Mark: - Outlets
    
    @IBOutlet weak var mTeamName: UILabel!
    @IBOutlet weak var mPlayed: UILabel!
    @IBOutlet weak var mPoint: UILabel!
    @IBOutlet weak var mWins: UILabel!
    @IBOutlet weak var mDraws: UILabel!
    @IBOutlet weak var mLose: UILabel!
    @IBOutlet weak var mGoals: UILabel!
    @IBOutlet weak var mGoalsAgainst: UILabel!
    @IBOutlet weak var mAverage: UILabel!
    @IBOutlet weak var mRank: UILabel!
    @IBOutlet weak var teamLogo: UIImageView!
    
    
    var destObject: [Int]?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let model = Model.sharedInstance.teams[(destObject?[0])!][(destObject?[1])!]
        
        //Initialize Labels with texts
        mTeamName.text = model.name
        mTeamName.adjustsFontSizeToFitWidth = true
        mPlayed.text = String(describing: model.played!)
        mPoint.text = String(describing: model.points!)
        mWins.text = String(describing: model.wins!)
        mDraws.text = String(describing: model.draws!)
        mLose.text = String(describing: model.defeits!)
        mGoals.text = String(describing: model.goalsfor!)
        mGoalsAgainst.text = String(describing: model.goalsagainst!)
        mAverage.text = String(describing: model.average!)
        mRank.text = String(describing: model.rank!)
        if(model.HasTeamLogo == true){
            let url = URL(string: model.imageURL!)
            teamLogo.kf.setImage(with: url)
        }else{
            teamLogo.image = UIImage(named: "default-team-logo-500")

        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
